def bubble_sort(data):
    sort_data = data.copy()
    n = len(data) - 1

    for i in range(len(data)):
        for j in range(n):
            if sort_data[j] > sort_data[j + 1]:
                sort_data[j], sort_data[j + 1] = sort_data[j + 1], sort_data[j]

    return sort_data


if __name__ == "__main__":
    data = list(map(int, input("enter 10 number: ").split()))

    sort_data = bubble_sort(data)
    print("bubble sort =", sort_data, sep=" ")
