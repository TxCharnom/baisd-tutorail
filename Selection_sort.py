# เลือกตัวมากสุดในตัวข้อมูล และสลับกับตัวหลังสุด


def selection_sort(num):
    sorted_num = num.copy()
    for x in range(len(num) - 1, 0, -1):
        max_num = 0

        for y in range(1, x + 1):
            if sorted_num[y] > sorted_num[max_num]:
                max_num = y
            sorted_num[x], sorted_num[max_num] = sorted_num[max_num], sorted_num[x]
    return sorted_num


if __name__ == "__main__":

    num = list(map(int, input("Enter 10 number: ").split()))
    sorted_num = selection_sort(num)
    print(sorted_num)
