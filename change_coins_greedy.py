def change(amount):

    ten_coin = 0
    five_coin = 0
    four_coin = 0
    two_coin = 0
    one_coin = 0

    while amount:

        if amount >= 10:
            amount -= 10
            ten_coin += 1
        elif amount >= 5:
            amount -= 5
            five_coin += 1
        elif amount >= 4:
            amount -= 4
            four_coin += 1
        elif amount >= 2:
            amount -= 2
            two_coin += 1
        elif amount >= 1:
            amount -= 1
            one_coin += 1
        else:
            break

    return f"10 coin = {ten_coin} coin\n5 coin = {five_coin} coin\n4 coin = {four_coin} coin\n2 coin = {two_coin} coin\n1 coin = {one_coin} coin"


if __name__ == "__main__":
    amount = int(input("Enter your amount: "))
    print(change(amount))
