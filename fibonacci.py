import timeit


def recur(x):

    if x < 2:
        return x
    else:
        return recur(x - 1) + recur(x - 2)


def bottom_up(x):
    fx = 0
    fy = 1
    fz = 1

    for i in range(2, x + 1):
        fz = fx + fy
        fx = fy
        fy = fz
    return fz


if __name__ == "__main__":
    x = int(input("Input integer: "))
    print(
        "recursive fibonacci:",
        recur(x),
        "Time is",
        timeit.timeit("recur(x)", globals=globals()),
    )
    print(
        "bottom_up fibonacci:",
        bottom_up(x),
        "Time is",
        timeit.timeit("bottom_up(x)", globals=globals()),
    )
